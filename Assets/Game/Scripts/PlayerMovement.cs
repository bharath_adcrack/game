using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class PlayerMovement : Movement
    {

        [SerializeField] PlayerAudio _playerAudio;

        PlayerControl _playerControl;

        [SerializeField] private AnimatorHandler _animator;
        [SerializeField] private float smoothTime = 1.0f;
        [SerializeField] private float maxSpeed = 2.0f;

        [SerializeField] private float _playerMovementSpeed = 2;
        [SerializeField] private CharacterController _characterController;
        

        public Vector2 current { get; private set; }
        Vector2 currentVelocity;



        #region Monobehaviour callbacks

        private void Awake()
        {
            _playerControl = new PlayerControl();
        }


        // Start is called before the first frame update
        void Start()
        {
            
        }


        private void OnEnable()
        {
            _playerControl.Enable();
        }

        private void OnDisable()
        {
            _playerControl.Disable();
        }

        // Update is called once per frame
        void Update()
        {
            Move();
        }
        #endregion


        #region Abstract methods
        public override void Move(Transform transform = null)
        {

            var moveOut = _playerControl.Player.Move.ReadValue<Vector2>();
            current = Vector2.SmoothDamp(current, moveOut, ref currentVelocity, smoothTime, maxSpeed);

            Vector3 move_;

            if(current != Vector2.zero)
            {
                Debug.Log($"Move {current}");
                move_ = new Vector3(current.x, 0, current.y);
                _characterController.Move(move_ * Time.deltaTime * _playerMovementSpeed);
                _animator.SetValues(current.x, current.y);
            }
            else
            {
                current = Vector2.zero;
            }
            

        }


        #endregion

        public void Audio()
        {
            // Debug.Log($"{nameof(AudioTest)}");
            _playerAudio.PlayFootStepSound();
        }
    
    }
}
