using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class AnimatorHandler : MonoBehaviour
    {



        private static int _horizontal = Animator.StringToHash("Horizontal");
        private static int _vertical = Animator.StringToHash("Vertical");


        public int Horizontal { get { return _horizontal; } }
        public int Vertical { get { return _vertical; } }

        [SerializeField] private Animator _animator;    


        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void SetValues(float horizontal , float vertical)
        {
            _animator.SetFloat(_horizontal, horizontal);
            _animator.SetFloat(_vertical, vertical);
        }
    }
}
