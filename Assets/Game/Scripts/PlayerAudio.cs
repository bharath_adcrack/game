using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class PlayerAudio : MonoBehaviour
    {


        [SerializeField] private PlayerMovement _playerMovement;

        [SerializeField] private AudioSource audioSource;

        [SerializeField] private AudioClip _footStepAudioClip;



        private void Awake()
        {
            
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        /// <summary>
        /// Play foot step sound
        /// </summary>
        public void PlayFootStepSound()
        {
            Debug.Log($"{nameof(PlayFootStepSound)}");
            // Manage volume according to the player current movement Y
            audioSource.volume = _playerMovement.current.y;

            // Assign an footstep audio clip
            audioSource.clip = _footStepAudioClip;

            // Play the audio clip at one shot.
            audioSource.PlayOneShot(audioSource.clip);
        }

    }
}
