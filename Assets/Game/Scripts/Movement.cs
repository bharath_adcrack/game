using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public abstract class Movement : MonoBehaviour
    {
        public abstract void Move(Transform transform = null);
    }
}
